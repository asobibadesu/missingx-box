import random
import time

ALP = [
    ["あ", "い", "う", "え", "お"],
    ["か", "き", "く", "け", "こ"],
    ["さ", "し", "す", "せ", "そ"],
    ["た", "ち", "つ", "て", "と"],
    ["は", "ひ", "ふ", "へ", "ほ"],
    ["ま", "み", "む", "め", "も"],
    ["ら", "り", "る", "れ", "ろ"],
    ["や", "ゆ", "よ", "ん", "ゐ", "わ", "ゑ", "を"],
]

selected_col = random.choice(ALP)
print("問題の行を選択中です。\n")
time.sleep(3)

if selected_col == ALP[7]:
    print("スペシャル問題です! や行・わ行 で古語を含みます. 百人一首で見たことがあるかもしれません。\n")
else:
    print(selected_col[0], "から", selected_col[-1], "までの ", selected_col[0], "行のひらがな です。\n")

x = random.choice(selected_col)
random.shuffle(selected_col)
alp = ""

for v in selected_col:
    if v != x:
        print(v, end=" ")

start = time.time()
ans = input("\n\n抜けている ひらがな は？")
ans = ans.strip(" ")

if ans == x:
    end = time.time()
    timer = format(end - start, "2f")
    print("正解です\n", "time: ", timer, "sec")
else:
    print("Try again!")
