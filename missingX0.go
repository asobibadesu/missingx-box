/*
脳トレ？
*/

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	// Create a data list. This example shows an i+i*i calculated list.
	var data []int

	for i := 1; i < 10; i++ {
		data = append(data, i + i * i)
	}

	// Display the range of the data list and create missingX.
	length := len(data)
	fmt.Printf("\nIn the following ordered list, you will find characters or numbers ranging from %d to %d. \n\n", data[0],data[length-1])
	x := data[rand.Intn(length)]

	for _, v := range data {
		if v != x {
			fmt.Print(v, " ")
		}
	}
	fmt.Println("\n\nWhat charactor or number is missing?")
	start := time.Now()  // Timer set

	// Desplay time and check the answer
	correctOrNot(x, start)
}

// correctOrNot function checks user input against the missing value.
func correctOrNot(x int, start time.Time) {
	var ans int
	fmt.Print("Your answer: ")
	_, err := fmt.Scan(&ans)
	if err != nil {
		fmt.Println("Error reading input: ", err)
		return
	}

	if ans == x {
		t := time.Now()

		fmt.Println("Correct!")
		fmt.Println("Time taken: ", t.Sub(start))
	} else {
		fmt.Printf("Oops! The answer is %d. Next!", x)
	}
}
